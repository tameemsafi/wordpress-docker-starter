# Wordpress Docker Starter

Allows you to develop wordpress websites using docker.

It includes:
1. Wordpress
2. Mysql 5.7
3. PHPMyAdmin

## Local setup

**You must have latest version of docker installed**

1. Pull this repo into a directory on your system
2. Run ```docker-composer up```
3. Start developing

## Wordpress

You will have access to all ```wp-content``` data as this is only you will need.

The rest is wordpress core and will no need to be changed.

You can access the wordpress install at the address below:

```
http://localhost:3000
```

## Mysql 5.7

You will have full database access and a clone of the data in ```database``` folder.

You should not need to make changes as everything can be done through the ```PHPMyAdmin``` interface.

## PHPMyAdmin

You will have access to ``PHPMyAdmin`` at the local address below.

```
http://localhost:4000
```